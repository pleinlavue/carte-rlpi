#!/usr/bin/env node
const fs = require('fs')
const papaparse = require('papaparse')
const asset = filename => fs.readFileSync(`./assets/${filename}`).toString()
const lib = filename => require(`../lib/${filename}`)
const csv = filename => papaparse.parse(asset(`${filename}.csv`), { header: true }).data

const addRanks = lib('add_ranks')
const getGroupsListHtml = lib('get_groups_list_html')

// Build public/data.js
var data = 'window.data = {}'
const addToData = (label, obj) => {
  data += `\nwindow.data.${label} = ${JSON.stringify(obj, null, 2)}`
}

const municipalities = csv('municipalities')
addRanks(municipalities)
addToData('municipalities', municipalities)

fs.writeFileSync('./public/data.js', data)

const groups = csv('groups')
const groupsListHtml = getGroupsListHtml(groups)

// Build public/index.html
const base = asset('base.html')
const lyonSvg = asset('lyon.svg')
const indexHtml = base
  .replace('<!-- lyon.svg -->', lyonSvg)
  .replace('<!-- groups list -->', groupsListHtml)

fs.writeFileSync('./public/index.html', indexHtml)
