# Carte RLPi

Ce dépôt [git](https://fr.wikipedia.org/wiki/Git) track le code source permettant de générer la page [https://pleinlavue.frama.io/carte-rlpi](https://pleinlavue.frama.io/carte-rlpi).

C'est un projet très petit faisant intervenir des connaissances en HTML, CSS, JS, git, et hébergement sur une GitLab Page : idéal si vous avez quelques bases pour en apprendre un peu plus ! Bonne explorations :)

## Dépendances
* [Node.js](http://nodejs.org) >= v8
### Dépendances optionelles
* Pour utiliser le script `watch`, `inotifywait` est nécessaire: `apt install inotify-tools`

## Installation
```sh
# Copiez le dépôt git
git clone https://framagit.org/pleinlavue/carte-rlpi
# Entrez dedant
cd carte-rlpi
# Installé les dépendances nécessaires au développement
npm install
# Mettez à jour le build
npm run build
# Lancez le serveur local
npm start
```

### Déveloper
Pour que le build se mette à jour à chaque changement des fichiers sources, si vous avez installé `inotifywait`, vous pouvez utiliser le [script `watch`](https://framagit.org/pleinlavue/carte-rlpi/blob/src/scripts/watch) :
```sh
npm run watch
```

## Créer la page Gitlab
Ici nous utilisons [framagit.org](http://framagit.org), l'instance Gitlab hébergé par [Framasoft](https://framasoft.org/)

* Créer un dépôt dédié à votre page
* Y créer un fichier `.gitlab-ci.yml` en entrant la commande suivante dans le terminal :

```sh
echo "pages:
  stage: deploy
  script:
  - echo 'Nothing to do...'
  artifacts:
    paths:
    - public
  only:
  - master
" > .gitlab-ci.yml
```

Voir la [doc sur les GitLab Pages](https://framagit.org/help/user/project/pages/index.md)

### Déployer

Pour que quelque chose apparaisse sur votre page :
* sauvegardez vos changement avec [`git commit`](https://git-scm.com/docs/git-commit)
* envoyez ces changements à votre instance GitLab: [`git push origin master`](https://git-scm.com/docs/git-push)

Dans notre cas, le développement ce faisant sur une autre branch ([`src`](https://framagit.org/pleinlavue/carte-rlpi/tree/src)), un petit [script de `deploy`](https://framagit.org/pleinlavue/carte-rlpi/blob/src/scripts/deploy) fait la tambouille nécessaire à déployer :
```sh
npm run deploy
```

## License
WTFPL