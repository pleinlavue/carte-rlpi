(function () {

  var colorByScore = {
    '-1': '#666',
    0: '#888',
    1: '#ff4400',
    2: '#ffcc00',
    3: '#ffff00',
    4: '#00ff00',
    5: '#179917'
  }

  var lyonCis = [
    '69123-1',
    '69123-2',
    '69123-3',
    '69123-4',
    '69123-5',
    '69123-6',
    '69123-7',
    '69123-8',
    '69123-9'
  ]

  var getElFromCi = function (ci) { return  document.getElementById('ci-' + ci) }

  var addMunicipalityStyle = function (municipalityData) {
    var ci = municipalityData['Code INSEE']
    var cis = ci === '69123' ? lyonCis : [ ci ]
    var municipalityEls = cis.map(getElFromCi)

    var rawScore = municipalityData['Note générale']

    var score = parseFloat(rawScore.replace(',', '.'))
    var roundedScore = Math.round(score)

    var name = municipalityData.Commune
    var rank = municipalityData.rank
    var maire = municipalityData.Maire
    var text = '<text> ' + name + ' | Maire : ' + maire + ' | Note : ' + rawScore + '/5 </text>'

    municipalityEls.forEach(function (el) {
      el.style.fill = colorByScore[roundedScore]
      el.innerHTML = text
    })

    if (ci === '69123') {
      // Hidding arrondissements lines by setting their color
      // to the same as Lyon in general
      var arrondissementsEls = document.querySelectorAll('.arrondissement')
      arrondissementsEls = toArray(arrondissementsEls)
      arrondissementsEls.forEach(function (arrondissement) {
        arrondissement.style.fill = colorByScore[roundedScore]
        arrondissement.style.stroke = colorByScore[roundedScore]
        arrondissement.innerHTML = text
      })
    }
  }

  var toArray = function (arrayLike) { return [].slice.call(arrayLike, 0)}

  var municipalities = window.data.municipalities

  municipalities.forEach(addMunicipalityStyle)

  var topMunicipalities = municipalities.slice(0, 5)
  var topMunicipalitiesList = document.getElementById('topMunicipalitiesList')

  var buildTopElement = function (municipalityData) {
    var rank = municipalityData.rank
    var name = municipalityData.Commune
    return '<li>' + rank + ' - ' + name + '</li>'
  }

  topMunicipalitiesList.innerHTML = topMunicipalities
    .map(buildTopElement)
    .join('')

  var tooltip = document.getElementById('tooltip')
  var lastTarget
  var lastChangeTime = 0

  var onMove = function (event) {
    var target = event.target

    // Debouncing
    var targetId = target && target.id
    var now = Date.now()
    if (lastTarget === targetId && now - lastChangeTime < 200) {
      console.log('debounced')
      return
    } else {
      lastTarget = targetId
      lastChangeTime = now
    }

    if (target && target.tagName === 'path') {
      tooltip.style.display = 'block'
      var text = target.textContent.trim()
      var commune = text.split('|')[0]
      text = text.replace(commune, '<strong>' + commune + '</strong>')
      tooltip.innerHTML = text
    } else {
      tooltip.style.display = 'none'
    }
  }

  document.onmousemove = onMove
  document.onscroll = onMove

})()
