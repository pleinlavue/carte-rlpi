const sortByScore = require('./sort_by_score')

module.exports = municipalities => {
  municipalities
  .sort(sortByScore)
  .forEach(addRank)
}

const addRank = (municipality, index) => {
  municipality.rank = index + 1
}
