module.exports = (a, b) => parseScore(b) - parseScore(a)

const parseScore = obj => {
  return parseFloat(obj['Note générale'].replace(',', '.'))
}
