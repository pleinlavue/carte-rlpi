const sortByScore = require('./sort_by_score')
const fs = require('fs')
const externalIcon = fs.readFileSync('./assets/external.svg')
  .toString()
  // Drop top comment
  .replace(/.*<svg/, '<svg')

module.exports = groups => {
  return groups
    .sort(sortByScore)
    .map(group => {
      const truncatedScoreClass = group['Note générale'].split(',')[0]
      const groupName = group['Groupe Politique']
      const filenameBase = getFilenameBase(groupName)
      const logo = getLogo(filenameBase)
      const response = getResponse(filenameBase)
      return `<tr>
        <td>${logo}</td>
        <td>${groupName}</td>
        <td>${group['Président·e de Groupe']}</td>
        <td class="score-${truncatedScoreClass}">${group['Note générale']}</td>
        <td class="response">${response}</td>
      </tr>`
    })
    .join('\n')
}

const getFilenameBase = groupName => {
  return groupName
    .toLowerCase()
    // .replace('et apparentés', '')
    // Remove text between parenthesis
    .replace(/\([\w\s]+\)/g, '')
    .replace(/'/g, ' ')
    .replace(/[,–]+/g, '')
    .replace(/\s+/g, ' ')
    .trim()
    .replace(/\s/g, '_')
}

const missingLogos = [
  'métropole_et_territoires'
]

const videoResponseLink = {
  'les_républicains_et_apparentés': 'http://www.lesrepublicains-metropolelyon.fr/christophe-quiniou-reglement-local-de-publicite/'
}

const missingResponses = [
  'les_républicains_et_apparentés',
  'métropole_et_territoires',
  'synergies-avenir'
]

const getLogo = filenameBase => {
  if (missingLogos.includes(filenameBase)) return ''
  return `<img class="logo" src="logos/${filenameBase}.jpg">`
}

const getResponse = filenameBase => {
  const videoLink = videoResponseLink[filenameBase]
  if (videoLink) return `<a href="${videoLink}" target="_blank">${externalIcon}video</a>`
  if (missingResponses.includes(filenameBase)) return ''
  return `<a href="reponses/${filenameBase}.pdf" target="_blank">${externalIcon}pdf</a>`
}
